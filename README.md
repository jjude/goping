# goping

Read a list of urls in csv and then ping them to find 200 or 404. Write results in csv.

## Getting started

1. Download binary for your OS
2. Create list of urls in a text file with name `urls.csv` in the same directory. Each row should contain only one url
3. Execute using `goping urls.csv`
4. Output will be in `urlpings.csv`

## Support
I created this for personal use. If you find this useful, feel free to use it. If you want more features or modifications, feel free to reach out to me, though I don't promise it will be done in the time-frame that you so desire.

## Roadmap
None

## Authors
Joseph Jude

## License
MIT license. Means, do whatever with the code. Just don't sue me.

## Project status
It does what I want the way I want.