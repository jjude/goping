package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

type Result struct {
	url        string
	err        string
	latency    time.Duration
	statusCode int
}

func ping(url string) Result {
	fmt.Println("Pinging: ", url)
	var r Result
	start := time.Now()
	if resp, err := http.Get(url); err != nil {
		//if there are issues in url string, it may not even ping
		if resp != nil {
			r = Result{url, err.Error(), 0, resp.StatusCode}
		} else {
			r = Result{url, err.Error(), 0, 0}
		}
	} else {
		t := time.Since(start).Round(time.Millisecond)
		resp.Body.Close()
		r = Result{url, "", t, resp.StatusCode}
	}
	return r
}

func main() {

	outfile, err := os.OpenFile("urlpings.csv", os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}
	defer outfile.Close()

	urlfile, err := os.Open("urls.csv")
	if err != nil {
		panic(err)
	}
	defer urlfile.Close()

	// read line by line
	csvReader := csv.NewReader(urlfile)

	for {
		rec, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		r := ping(rec[0])
		if _, err := outfile.WriteString(r.url + "," + strconv.Itoa(r.statusCode) + "," + r.err + "," + r.latency.String() + "\n"); err != nil {
			fmt.Println("Error writing to output: ", err)
		}
	}
}
